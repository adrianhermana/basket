<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clubs".
 *
 * @property int $id
 * @property string $nombre
 * @property string $telefono
 * @property string $email
 * @property string $correo
 * @property string $nombre_responsable
 * @property string $apellido_responsable
 * @property string $telefono_responsable
 * @property string $licencia_club
 *
 * @property Equipos[] $equipos
 */
class Clubs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clubs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'telefono', 'email', 'correo', 'nombre_responsable', 'apellido_responsable', 'telefono_responsable', 'licencia_club'], 'required'],
            [['nombre', 'nombre_responsable', 'apellido_responsable'], 'string', 'max' => 55],
            [['telefono', 'telefono_responsable'], 'string', 'max' => 13],
            [['email'], 'string', 'max' => 85],
            [['correo'], 'string', 'max' => 10],
            [['licencia_club'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'correo' => 'Correo',
            'nombre_responsable' => 'Nombre Responsable',
            'apellido_responsable' => 'Apellido Responsable',
            'telefono_responsable' => 'Telefono Responsable',
            'licencia_club' => 'Licencia Club',
        ];
    }

    /**
     * Gets query for [[Equipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasMany(Equipos::className(), ['id_clubs' => 'id']);
    }
}
