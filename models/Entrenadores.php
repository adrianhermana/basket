<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property int $id_etrenadores
 * @property string $nombre
 * @property string $apellido
 * @property string $dni
 * @property string $licencia
 * @property string $cod_licencia
 * @property string $email
 * @property string $telefono
 * @property string|null $temporada
 *
 * @property Equipos[] $equipos
 * @property JugadoresEntrenadoresEquipos[] $jugadoresEntrenadoresEquipos
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'dni', 'licencia', 'cod_licencia', 'email', 'telefono'], 'required'],
            [['nombre', 'apellido'], 'string', 'max' => 55],
            [['dni', 'temporada'], 'string', 'max' => 9],
            [['licencia'], 'string', 'max' => 1],
            [['cod_licencia'], 'string', 'max' => 4],
            [['email'], 'string', 'max' => 85],
            [['telefono'], 'string', 'max' => 13],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_etrenadores' => 'Id Etrenadores',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'dni' => 'Dni',
            'licencia' => 'Licencia',
            'cod_licencia' => 'Cod Licencia',
            'email' => 'Email',
            'telefono' => 'Telefono',
            'temporada' => 'Temporada',
        ];
    }

    /**
     * Gets query for [[Equipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasMany(Equipos::className(), ['id_entrenadores' => 'id_etrenadores']);
    }

    /**
     * Gets query for [[JugadoresEntrenadoresEquipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadoresEntrenadoresEquipos()
    {
        return $this->hasMany(JugadoresEntrenadoresEquipos::className(), ['id_entrenadores' => 'id_etrenadores']);
    }
}
