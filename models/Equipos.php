<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property int $id
 * @property int $id_entrenadores
 * @property int $id_jugadores
 * @property int $id_clubs
 * @property int $id_ligas
 * @property string $nombre
 * @property string $licencia_equipo
 *
 * @property Entrenadores $entrenadores
 * @property Jugadores $jugadores
 * @property Clubs $clubs
 * @property Ligas $ligas
 * @property JugadoresEntrenadoresEquipos[] $jugadoresEntrenadoresEquipos
 */
class Equipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_entrenadores', 'id_jugadores', 'id_clubs', 'id_ligas', 'nombre', 'licencia_equipo'], 'required'],
            [['id_entrenadores', 'id_jugadores', 'id_clubs', 'id_ligas'], 'integer'],
            [['nombre'], 'string', 'max' => 55],
            [['licencia_equipo'], 'string', 'max' => 4],
            [['id_entrenadores'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['id_entrenadores' => 'id_etrenadores']],
            [['id_jugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['id_jugadores' => 'id']],
            [['id_clubs'], 'exist', 'skipOnError' => true, 'targetClass' => Clubs::className(), 'targetAttribute' => ['id_clubs' => 'id']],
            [['id_ligas'], 'exist', 'skipOnError' => true, 'targetClass' => Ligas::className(), 'targetAttribute' => ['id_ligas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_entrenadores' => 'Id Entrenadores',
            'id_jugadores' => 'Id Jugadores',
            'id_clubs' => 'Id Clubs',
            'id_ligas' => 'Id Ligas',
            'nombre' => 'Nombre',
            'licencia_equipo' => 'Licencia Equipo',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasOne(Entrenadores::className(), ['id_etrenadores' => 'id_entrenadores']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasOne(Jugadores::className(), ['id' => 'id_jugadores']);
    }

    /**
     * Gets query for [[Clubs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClubs()
    {
        return $this->hasOne(Clubs::className(), ['id' => 'id_clubs']);
    }

    /**
     * Gets query for [[Ligas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLigas()
    {
        return $this->hasOne(Ligas::className(), ['id' => 'id_ligas']);
    }

    /**
     * Gets query for [[JugadoresEntrenadoresEquipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadoresEntrenadoresEquipos()
    {
        return $this->hasMany(JugadoresEntrenadoresEquipos::className(), ['id_equipos' => 'id']);
    }
}
