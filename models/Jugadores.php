<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $id
 * @property string $dni
 * @property string $nombre
 * @property string $apellidos
 * @property string $cod_licencia
 * @property string $fecha_licencia
 * @property string $caducidad_licencia
 * @property string $temporada
 * @property string $email
 *
 * @property Equipos[] $equipos
 * @property Telefono $id0
 * @property JugadoresEntrenadoresEquipos[] $jugadoresEntrenadoresEquipos
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'nombre', 'apellidos', 'cod_licencia', 'fecha_licencia', 'caducidad_licencia', 'temporada', 'email'], 'required'],
            [['fecha_licencia', 'caducidad_licencia'], 'safe'],
            [['dni', 'temporada'], 'string', 'max' => 9],
            [['nombre', 'apellidos'], 'string', 'max' => 55],
            [['cod_licencia'], 'string', 'max' => 4],
            [['email'], 'string', 'max' => 85],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Telefono::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'cod_licencia' => 'Cod Licencia',
            'fecha_licencia' => 'Fecha Licencia',
            'caducidad_licencia' => 'Caducidad Licencia',
            'temporada' => 'Temporada',
            'email' => 'Email',
        ];
    }

    /**
     * Gets query for [[Equipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasMany(Equipos::className(), ['id_jugadores' => 'id']);
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Telefono::className(), ['id' => 'id']);
    }

    /**
     * Gets query for [[JugadoresEntrenadoresEquipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadoresEntrenadoresEquipos()
    {
        return $this->hasMany(JugadoresEntrenadoresEquipos::className(), ['id_jugadores' => 'id']);
    }
}
