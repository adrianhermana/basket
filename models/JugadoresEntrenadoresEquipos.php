<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores_entrenadores_equipos".
 *
 * @property int $id
 * @property int $id_jugadores
 * @property int $id_entrenadores
 * @property int $id_equipos
 *
 * @property Entrenadores $entrenadores
 * @property Equipos $equipos
 * @property Jugadores $jugadores
 */
class JugadoresEntrenadoresEquipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores_entrenadores_equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_jugadores', 'id_entrenadores', 'id_equipos'], 'required'],
            [['id_jugadores', 'id_entrenadores', 'id_equipos'], 'integer'],
            [['id_entrenadores'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['id_entrenadores' => 'id_etrenadores']],
            [['id_equipos'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['id_equipos' => 'id']],
            [['id_jugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['id_jugadores' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_jugadores' => 'Id Jugadores',
            'id_entrenadores' => 'Id Entrenadores',
            'id_equipos' => 'Id Equipos',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasOne(Entrenadores::className(), ['id_etrenadores' => 'id_entrenadores']);
    }

    /**
     * Gets query for [[Equipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasOne(Equipos::className(), ['id' => 'id_equipos']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasOne(Jugadores::className(), ['id' => 'id_jugadores']);
    }
}
