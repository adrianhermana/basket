<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $id
 * @property int $id_liga
 * @property string $Clasificacion_local
 * @property string $Clasificacion_visitante
 * @property string $Nombre_local
 * @property string $Nombre_visitante
 * @property string $Marcador_local
 * @property string $Marcador_visitante
 * @property string $lugar_partido
 * @property string $fecha_realizacion
 *
 * @property Ligas $liga
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_liga', 'Clasificacion_local', 'Clasificacion_visitante', 'Nombre_local', 'Nombre_visitante', 'Marcador_local', 'Marcador_visitante', 'lugar_partido', 'fecha_realizacion'], 'required'],
            [['id_liga'], 'integer'],
            [['fecha_realizacion'], 'safe'],
            [['Clasificacion_local', 'Clasificacion_visitante'], 'string', 'max' => 10],
            [['Nombre_local', 'Nombre_visitante'], 'string', 'max' => 55],
            [['Marcador_local', 'Marcador_visitante'], 'string', 'max' => 3],
            [['lugar_partido'], 'string', 'max' => 30],
            [['id_liga'], 'exist', 'skipOnError' => true, 'targetClass' => Ligas::className(), 'targetAttribute' => ['id_liga' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_liga' => 'Id Liga',
            'Clasificacion_local' => 'Clasificacion Local',
            'Clasificacion_visitante' => 'Clasificacion Visitante',
            'Nombre_local' => 'Nombre Local',
            'Nombre_visitante' => 'Nombre Visitante',
            'Marcador_local' => 'Marcador Local',
            'Marcador_visitante' => 'Marcador Visitante',
            'lugar_partido' => 'Lugar Partido',
            'fecha_realizacion' => 'Fecha Realizacion',
        ];
    }

    /**
     * Gets query for [[Liga]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLiga()
    {
        return $this->hasOne(Ligas::className(), ['id' => 'id_liga']);
    }
}
