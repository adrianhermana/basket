<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresEntrenadoresEquipos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jugadores-entrenadores-equipos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_jugadores')->textInput() ?>

    <?= $form->field($model, 'id_entrenadores')->textInput() ?>

    <?= $form->field($model, 'id_equipos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
