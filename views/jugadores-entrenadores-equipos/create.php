<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresEntrenadoresEquipos */

$this->title = 'Create Jugadores Entrenadores Equipos';
$this->params['breadcrumbs'][] = ['label' => 'Jugadores Entrenadores Equipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-entrenadores-equipos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
