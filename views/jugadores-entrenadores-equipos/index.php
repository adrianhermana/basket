<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jugadores Entrenadores Equipos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-entrenadores-equipos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Jugadores Entrenadores Equipos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_jugadores',
            'id_entrenadores',
            'id_equipos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
