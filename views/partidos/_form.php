<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Partidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partidos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_liga')->textInput() ?>

    <?= $form->field($model, 'Clasificacion_local')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Clasificacion_visitante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nombre_local')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nombre_visitante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Marcador_local')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Marcador_visitante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lugar_partido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_realizacion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
