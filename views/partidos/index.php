<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Partidos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_liga',
            'Clasificacion_local',
            'Clasificacion_visitante',
            'Nombre_local',
            //'Nombre_visitante',
            //'Marcador_local',
            //'Marcador_visitante',
            //'lugar_partido',
            //'fecha_realizacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
